push!(LOAD_PATH,"../src/")
using GePixDet
using Documenter
makedocs(
         sitename = "GePixDet.jl",
         modules  = [GePixDet],
         pages=[
                "Home" => "index.md"
               ])
deploydocs(;
    repo="gitlab.synchrotron-soleil.fr/detecteurs/leaps-innov/src/GePixDet.jl",
)
