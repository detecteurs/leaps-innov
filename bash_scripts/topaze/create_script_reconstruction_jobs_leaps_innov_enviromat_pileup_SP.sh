#!/bin/sh
impurity=03
zsample=50
HV=200
nPU=3
interpixel=400
polarization=99
echo "impurity of $impurity"
echo "HV of $HV"
for peaking_time in 125   
do
	for file_n in {1..9..1} 
	do
		for pixel_size in 4
		do
			for Energy in 32 
			do
				echo ${pixel_size}
				FILE="ssd_leaps_innov_SP_pixels_z_${zsample}_energy_${Energy}_keV_HV_${HV}V_pt_${peaking_time}_run${file_n}.jl"
				/bin/cat <<EOM >$FILE

using Plots
using DelimitedFiles
using SpecialFunctions
using Unitful, UnitfulAtomic
using HDF5, CSV, DataFrames
using LegendHDF5IO
using SolidStateDetectors
using ImageFiltering, Noise, DSP, RadiationDetectorSignals
using Random, Distributions,StatsBase, LinearAlgebra
using GePixDet
T = Float32;

d = Normal(0,6)
x = rand(d, 10^6)
h = fit(Histogram, x, -20.5:1.0:20.5)
h_n=normalize(h, mode=:pdf)
transfer_function = vcat(zeros(1),h_n.weights[1:end])

baselines = h5read("/ccc/work/cont003/soleil/manzanil/SSD/Real_baseline.hdf5", "baselines")
println("baselines OK")
ssd_sim_result=ssd_read("/ccc/work/cont003/soleil/manzanil/SSD/LEAPS_INNOV_mirion_size_small_pixel_interpixel_400um_impurity_03_HV_200V_1pc_batch.h5", Simulation)
println("SSD files OK")

base_dir = "/ccc/work/cont003/soleil/manzanil/EnviroMAT/Geant4_output/gamma_${Energy}_keV_x_source_0mm_GeThickness_4mm_setup_4_collimator_tungsten_alloy_3mm/"
files = filter(x->occursin("LEAPS_INNOV_leaps_SP_enviroMAT_z_${zsample}mm_pol_90d_99pc_run_${file_n}",x), readdir(base_dir))
Rec_files = base_dir.*files
datos =Vector{}(undef,length(Rec_files))
for i=1:1:length(Rec_files) 
        datos[i] = CSV.read(Rec_files[i],DataFrame, header=[:evt, :x, :y, :z, :E],skipto=10)
end
df_g4 = vcat(datos...);

println("geant4 files OK")

ro_contacts = length(ssd_sim_result.weighting_potentials) - 2


function get_evts_index(df::DataFrame)
    event_number = df[1,:evt]
    evt_coutner = 1
    evts_info = []
    start_evt_index = 1
    for i = 1 : 1 : length(df[!,:evt])
        if  event_number == df[i,:evt]
            continue
        else
            end_evt_index = i - 1
            id_nxt_evt = df[i,:evt]
            event_number = df[i,:evt]
            push!(evts_info,[evt_coutner start_evt_index end_evt_index])
            evt_coutner += 1
            start_evt_index = i
        end
    end
    return vcat(evts_info...)
end


function get_wfs_evt(df::DataFrame)
    wf_evt = Array{ArrayOfRDWaveforms}[]
    time_step = 4u"ns"
    event_coordinates = CartesianPoint{T}[]
    event_energy = []
    total_energy = []
    hit_xyz = Array{T}(undef, 0, 3)
    E_evt = 0
    for i = 1 : 1 : length(df[!,:evt])
        energy = df[i,:E]
        hit_coordinates = CartesianPoint{T}(df[i,:x]/1000,df[i,:y]/1000,(df[i,:z])/1000)
        push!(event_coordinates,hit_coordinates)
        push!(event_energy,energy)
    end
    E_event = event_energy*u"eV"
    evt = Event(event_coordinates, E_event)
    E_evt = sum(event_energy)
    try
        simulate!(evt, ssd_sim_result,Δt = time_step,diffusion = true, self_repulsion = true)
        wf_evt = evt.waveforms[2:11]
    catch
        starting_positions = [ CartesianPoint{T}( 0.0, 0.0, 0.0 )]
        energy_depos = T[0.1] * u"keV"
        evt = Event(starting_positions, energy_depos);
        simulate!(evt, ssd_sim_result,Δt = time_step,diffusion = true, self_repulsion = true)
        wf_evt = evt.waveforms[2:11]
        E_evt = 0.1
    end
    return wf_evt, E_evt
end


println("function get wfs OK")

function get_values_pu(df::DataFrame,n_pile_ups::Int)
    Index_evts = get_evts_index(df)
    time_evt = []
    pixel_charge = []
    total_energy = []
    pixels_ID = []
    section_ID = []
    wf_evt = []
    ro_pixels = 10
    section = 0
    #println("last evt: ",Index_evts[end,1])
    #wf_debug = []
    for evt_counter = 1 : n_pile_ups :  Index_evts[end,1] - n_pile_ups + 1
        wfs_all = []
        E_true_evts = zeros(n_pile_ups)
        section += 1
        second_counter = evt_counter 
        #get waveforms of all events
        for first_evt = 1 : 1 : n_pile_ups
            start_evt = Index_evts[second_counter,2]
            finish_evt = Index_evts[second_counter,3]
            #println("evt counter: ",evt_counter," second counter: ",second_counter," start: ",start_evt," ends: ",finish_evt)
            wf_evt, E_true_evts[first_evt] = get_wfs_evt(df[start_evt:finish_evt,:])
            second_counter += 1
            push!(wfs_all,wf_evt)
            
        end
        pu_wfs = wfs_all[1]
        shift =  rand(Poisson(80),1)[1] + length(pu_wfs[1].signal)  
        for n_evt = 2 : 1 : n_pile_ups
            pu_wfs_test = wfs_all[n_evt]
            pu_wfs = add_PileUp.(pu_wfs,wfs_all[n_evt],shift)
            shift = length(pu_wfs[1].signal) + rand(Poisson(80),1)[1]
        end
        #Add tail and baseline
        wfs_with_tail = add_baseline_and_extend_tail.(pu_wfs,250,1000)
        wfs_with_tail_amp = add_amplification.(wfs_with_tail,tuple(transfer_function))
        #Add electronics noise
        base_ids = rand(1:length(baselines[:,1]),ro_pixels)
        y_noise = []
        for i in base_ids
            push!(y_noise,baselines[i,:]) 
        end
        wfs_with_tail_amp_noise = add_electronics_noise.(wfs_with_tail_amp,y_noise)
        #Get energies
        pixel_trap = trap_norm.(wfs_with_tail_amp_noise,${peaking_time},10,true)
        evts_in_wf = find_peaks.(pixel_trap;minpeakheight=350,minpeakdistance=10,threshold=50)
        amplitudes = []
        positions = []
        pixel_ids = []
        for i = 1:length(evts_in_wf)
            if length(evts_in_wf[i]) == 0
                continue
            else
                amplitude = getproperty.(evts_in_wf[i], :value)
                position = getproperty.(evts_in_wf[i], :idx)
                pixel_id = repeat([i], length(amplitude))
                push!(amplitudes,amplitude)
                push!(positions,position)
                push!(pixel_ids,pixel_id)
            end
        end
        pixel_energies = vcat(amplitudes...)
        pixel_sections = fill(section, length(pixel_energies))
        t_evt = vcat(positions...)
        id_pixels = vcat(pixel_ids...)
        #wf_debug = pixel_trap
        ##end pile up
        if length(pixel_energies) > 0
            push!(time_evt,t_evt)
            push!(pixel_charge,pixel_energies)
            push!(pixels_ID,id_pixels)
            push!(total_energy,E_true_evts)
            push!(section_ID,pixel_sections)
        end
    end
    #return wf_debug
    return data_to_save = [total_energy pixel_charge time_evt pixels_ID section_ID]
end

println("function process file OK")

data_to_save = get_values_pu(df_g4,${nPU});

println("file processed succesfully")

A_E_true = vcat(data_to_save[:,1]...)
A_E_ADC = vcat(data_to_save[:,2]...)
A_evts_time = vcat(data_to_save[:,3]...)
A_evts_pixels = vcat(data_to_save[:,4]...)
A_sections = vcat(data_to_save[:,5]...)

if length(A_E_ADC) > 5
	h5open("/ccc/work/cont003/soleil/manzanil/EnviroMAT/reconstruction/reconstruction_leaps_innov_SP_enviroMAT_polarization_${polarization}_z_${zsample}_MoreZrRb_pileup_${nPU}_E_${Energy}keV_pt_${peaking_time}_file_${file_n}.h5", "w") do file
		write(file, "A_E_ADC", A_E_ADC)
		write(file, "A_E_true", A_E_true)
		write(file, "A_evts_time", A_evts_time)
		write(file, "A_evts_pixels", A_evts_pixels)
		write(file, "A_sections", A_sections)
	end
end

println("file saved succesfully")

EOM
			FILEJOB="job_leaps_innov_SP_enviromat_z_${zsample}_E_${Energy}_impurity_${impurity}_HV_${HV}_pt_${peaking_time}_n1_${file_n}.sh"
	
        	        /bin/cat <<EOM >$FILEJOB
#!/bin/bash
set -x
#MSUB -r MyJob_case_${interpixel} # Request name
#MSUB -n 1 # Total number of processes to use
#MSUB -c 128 # Number of cores per process to use
#MSUB -T 85000 # Elapsed time limit in seconds
#MSUB -o /ccc/work/cont003/soleil/manzanil/EnviroMAT/start_jobs/logs/enviro_leaps_%I.o # Standard output. %I is the job id
#MSUB -e /ccc/work/cont003/soleil/manzanil/EnviroMAT/start_jobs/logs/enviro_leaps_%I.e # Error output. %I is the job id
#MSUB -q milan # Choosing standard nodes
#MSUB -m work,scratch # Mount files systems
export OMP_NUM_THREADS=128
source /ccc/cont003/home/soleil/manzanil/setup_ssd_jobs.sh
ccc_mprun julia ${FILE} 
exit 0
EOM
ccc_msub $FILEJOB
			done
		done
	done

done
