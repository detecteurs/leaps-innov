"""
     get_DANTE_data(file_name::String)
Function to get data from a hdf file produced by  DANTE 
It takes the file name (whole path included) and returns an array contianing the data from the 8 channels being readout by DANTE 
"""
function get_DANTE_data(file_name::String)
    h5_file = h5open(file_name,"r")
    obj = h5_file["Waveform"]
    metadata = read(obj)
    wfs_data = metadata["Waves"]
    close(h5_file)
    return Int.(wfs_data)
end


"""
     get_DATE_wf_mode(dante_file::String)
Function to get raw data when using script to save data in a h5 file
It will return a matrix containing the data of all channels in the file
"""
function get_DANTE_wf_mode(dante_file::String)
    h5_file = h5open(dante_file,"r")
    obj = h5_file["dataset_1"]
    datos = read(obj)
    close(h5_file)
    return datos
end


"""
     get_DANTE_decimation(file::String)
Function to get the decimation of sample saved by DANTE
It will return a vector with the decimation fo each channel
"""
function get_DANTE_decimation(file::String)
    h5_file = h5open(file,"r")
    obj = h5_file["Configuration"]
    metadata = read(obj)
    decimate = metadata["Decimation_Ratio"]
    close(h5_file)
    return Int.(decimate)
end


"""
     get_Event_positions(wv::Vector{Int64})
Function to get position of the events from a long waveform porduced by  DANTE 
It takes a vector like a waveform and subtract the baseline using the first 10 samples 
It returns the position i.e sample numbers where the events takes place  
"""
function get_Event_positions(wf::Vector{<:Real},trap_rise::Int = 5, trap_flat::Int = 3,peak_threshold = 6.)
    myvector_baseline = wf[1:end-1].- mean(wf[1:10])
    trap_data = trap_norm(myvector_baseline,trap_rise,trap_flat,true)
    peak_wf, pos_peaks = RadiationSpectra.peakfinder(trap_data, threshold=peak_threshold, markov=false)
    return sort(trunc.(Int, pos_peaks))
end

#function get_Event_positions(wf::Vector{AbstractFloat},trap_rise::Int = 5, trap_flat::Int = 3,peak_threshold = 6.)
#    myvector_baseline = wf[1:end-1].- mean(wf[1:10])
#    trap_data = trap_norm(myvector_baseline,trap_rise,trap_flat,true)
#    peak_wf, pos_peaks = RadiationSpectra.peakfinder(trap_data, threshold=peak_threshold, markov=false)
#    return sort(trunc.(Int, pos_peaks))
#end
"""
     get_restart_wf(wf::Vector{Int64},step_down::Int=300)
Function to get position of reset of a long waveform obtained with DANTE 
It takes the waveform of channel with step_down and returns the first position of the sample where the rest started 
"""
function get_restart_wf(wf::Vector{Int64},step_down::Int=300)
    positions = Int[]
    locations = Int[]
    for i=2:1:length(wf)
        if (wf[i] + step_down) < wf[i-1]
            push!(locations,i)
        end
    end
    push!(positions,locations[1])
    for i=2:1:length(locations)
        if locations[i] - locations[i-1] > 500
            push!(positions,locations[i])
        end
    end
    return positions
end
"""
     get_restart_wf(wf::Vector{Int32},step_down::Int=300)
Function to get position of reset of a long waveform obtained with DANTE 
It takes the waveform of channel with step_down and returns the first position of the sample where the rest started 
"""
function get_restart_wf(wf::Vector{Int32},step_down::Int=300)
    positions = Int[]
    locations = Int[]
    for i=2:1:length(wf)
        if (wf[i] + step_down) < wf[i-1]
            push!(locations,i)
        end
    end
    push!(positions,locations[1])
    for i=2:1:length(locations)
        if locations[i] - locations[i-1] > 500
            push!(positions,locations[i])
        end
    end
    return positions
end

"""
 get_restart_wf_trap(wf_in::Vector{<:Real}, threshold::Real)
Function to get the restart positions of waveform acquired with DANTE 
It accepts a waveform and a threshold and returns the postions of the re-starts
it assumes the ramps are going up, if it's not the case just invert the waveforms
"""
function get_restart_wf_trap(wf_in::Vector{<:Real}, threshold::Real)
    wf_baseline = wf_in .- mean(wf_in[1:5])
    wf = trap_norm(wf_baseline,50,3,true)
    positions = Int[] 
    for i = 2 : 1 : length(wf) - 1
        if wf[i] < -threshold && wf[i] < wf[i-1] && wf[i] < wf[i+1]
            push!(positions,i)
        end
    end
    return positions
end

"""
      correct_leakage_current(wf::Vector{T}, slope::{Real})
Function to make leakage current correction
It accepts a waveform and the slope of the leakage current previously measured 
It returns a new waveform with flat baselines
"""
function correct_leakage_current(wf::Vector{<:Integer}, slope::Float64) 
    new_signal = Vector{AbstractFloat}(undef, length(wf))
    new_signal[1] = wf[1]
    for i =2:1:length(wf)
        new_signal[i] =  wf[i] - i * slope
    end
    return new_signal
end
