"""
    get_rising_time(wvs::RDWaveform,wf_starts_at = 0.10,wf_ends_at = 0.90) 
Accepts a RDWaveform and return the rising time 
A low and high level can be given as input 
The default corresponds to 10 to 90 % of the maximum of the waveform 
"""
function get_rising_time(wvs::RDWaveform,wf_starts_at = 0.10,wf_ends_at = 0.90)
    wf_Max = wvs.signal[argmax(wvs.signal)]
    wf_Min = wvs.signal[argmin(wvs.signal)]
    polarity::Int = 0
    
    ## check polarity of waveform
    if abs(wf_Max) > abs(wf_Min)
        polarity = 1
        wf_Max_pc_low = wf_Max * wf_starts_at * polarity
        wf_Max_pc_high = wf_Max * wf_ends_at * polarity
    else
        polarity = -1
        wf_Max_pc_low = wf_Min * wf_starts_at * polarity
        wf_Max_pc_high = wf_Min * wf_ends_at * polarity
    end
    
    ## make sure waveform is inverted if polarity is negative
    wf_abs = wvs.signal * polarity
    
    rising_starts = -99u"ns"
    rising_ends = -99u"ns"
    i_pc_low = 1
    
    ## find low % position in the waveform
    for i=1:length(wf_abs)
        if wf_abs[i] > wf_Max_pc_low
            rising_starts = wvs.time[i]
            i_pc_low = i
            break
        end
    end
    
    ## find high % position in the waveform
    ## start searching after low % position to gain in performance
    for i=i_pc_low:length(wf_abs)
        if wf_abs[i] > wf_Max_pc_high
            rising_ends = wvs.time[i]
            break
        end
    end
    ## return the rising time defined as the time required to go from 5% to  95% of the maximum value in the waveform
    return rising_time = rising_ends - rising_starts
end

"""
    get_rising_time(wvs::AbstractVector,wf_starts_at = 0.10,wf_ends_at = 0.90) 
Accepts a vector containing a waveform and return the rising time 
A low and high level can be given as input 
The default corresponds to 10 to 90 % of the maximum of the waveform 
"""
function get_rising_time(wvs::AbstractVector,wf_starts_at = 0.10,wf_ends_at = 0.90)
    wf_Max = wvs[argmax(wvs)]
    wf_Min = wvs[argmin(wvs)]
    polarity::Int = 0
    
    ## check polarity of waveform
    if abs(wf_Max) > abs(wf_Min)
        polarity = 1
        wf_Max_pc_low = (wf_Max - wf_Min) * wf_starts_at * polarity + wf_Min
        wf_Max_pc_high = (wf_Max - wf_Min) * wf_ends_at * polarity + wf_Min
    else
        polarity = -1
        wf_Max_pc_low = -1 * ( (wf_Min - wf_Max) * wf_starts_at * polarity - wf_Max)
        wf_Max_pc_high = -1 * ( (wf_Min - wf_Max) * wf_ends_at * polarity - wf_Max )
    end
    
    ## make sure waveform is inverted if polarity is negative
    wf_abs = wvs * polarity
    
    rising_starts = -99
    rising_ends = -99
    i_pc_low = 1
    
    ## find low % position in the waveform
    for i=1:length(wf_abs)
        if wf_abs[i] > wf_Max_pc_low
            rising_starts = i
            i_pc_low = i
            break
        end
    end
    
    ## find high % position in the waveform
    ## start searching after low % position to gain in performance
    for i=i_pc_low:length(wf_abs)
        if wf_abs[i] > wf_Max_pc_high
            rising_ends = i
            break
        end
    end
    ## return the rising time in samples defined as the time required to go from 5% to  95% of the maximum value in the waveform
    return rising_time = rising_ends - rising_starts
end


"""
    get_pixels_info(wf_energies::AbstractVector, threshold = 100.0) 
Accepts an array with info of pixel energies and return the pixel hitted 
corresponding to the pixel with the highest collected charge 
It also returns the total charge, and number of pixels that has collected some charge above a certain threshold (default 100 e) 
"""
function get_pixel_hitted(wf_energies::AbstractVector, threshold::T = 250.0) where {T}
    pixel_number::Int = 0
    absolute_max = 0
    total_charge = 0
    nPixels::Int = 0
    for i=1:length(wf_energies) 
        pixel_energy = wf_energies[i] 
        if pixel_energy > threshold
            total_charge += pixel_energy
            nPixels +=1
        end
        if pixel_energy > absolute_max
            absolute_max = pixel_energy
            pixel_number = i
        end
    end
    return pixel_number, absolute_max, total_charge, nPixels
end
"""
     get_next_dt(Φ::Float64,prob::Float64)
Function to get the time of next event given a flux Φ in photons/s and a probability prob that one of these photons interact in the detector.
The prob will depend on the physics case, and can be estimated/determined using geant4 simulations. 
An example for the SAMBA beamline: 
A flux of photons of 32 keV hit a sample (EnviroMAT), the elements in the sample will produce fluorescence photons that can interact in the detector. Another possibility is elastic scattering. 
Geant4 simulations gives a prob~0.13% (0.0013) of having a photon interacting in the detector per photon of the beam (either from fluo or elastic scattering)
"""
function get_next_dt(Φ::Float64,prob::Float64)
    Δt = 0u"ns"
    period = 1181u"ns" #at soleil 
    bunchs = 416 #uniform mode at soleil
    time_between_bunchs = period/bunchs
    bunchs_per_second = 1e9u"ns" / time_between_bunchs
    #compute probability to have a photon in detector for each bunch
    #prob previously estimated with geant4 or other, if direct beam then prob ~ 1.0
    #but should be in general lower since some photons can pass teh detector without interacting, then just use geant4
    proba_per_bunch = Φ * prob / bunchs_per_second 
    if proba_per_bunch < 1e-5
        @warn("probability or Φ seems too low, this might result in a very long coputation time, consider increasing Φ or prob")
    end
    #loop until a photon hit the detector
    #Add in each step the distance between bunchs
    #Could be modified to take into account the hybrid mode
    while proba_per_bunch < rand()
        Δt += time_between_bunchs
    end
    return Δt
end
