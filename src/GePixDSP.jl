"""
    add_baseline_and_extend_decay_tail(wv::RDWaveform, n_baseline_samples::Int, total_waveform_length::Int, decay_constant::T)
Function to add baseline previous to the simulated SSD waveforms and a exponential decay tail
It accepts a RDWaveform and ints to indicate the baseline to add and the total length of the waveform  
the decay tail length will depend of the decay_constant.signal in ns, typically of the order of few thousands of ns, example: 5000
It returns a new waveform with baseline and decay tail  
"""
function add_baseline_and_extend_decay_tail(wv::RDWaveform, n_baseline_samples::Int, total_waveform_length::Int, decay_constant::T) where {T}
    new_signal = Vector{T}(undef, total_waveform_length)
    new_signal[1:n_baseline_samples] .= ustrip(wv.signal[1])
    sample_to_time::T = ustrip(step(wv.time))
    if length(wv.signal) <= total_waveform_length - n_baseline_samples
        new_signal[n_baseline_samples+1:n_baseline_samples+length(wv.signal)] = ustrip(wv.signal)
        for sample = n_baseline_samples+length(wv.signal)+1:1:total_waveform_length
            decay_ratio = @fastmath exp((ustrip(wv.time[end]) + n_baseline_samples * sample_to_time - sample * sample_to_time)/decay_constant)
            new_signal[sample] = ustrip(wv.signal[end])*decay_ratio
        end
    else
        new_signal[n_baseline_samples+1:end] = ustrip(wv.signal[1:total_waveform_length - n_baseline_samples])
    end
    new_times = range( zero(first(wv.time)), step = step(wv.time), length = total_waveform_length )
    return RDWaveform( new_times, new_signal.*u"e_au" )
end


"""
    add_amplification(ssd_wf::RDWaveform, channel_transfer_function::AbstractVector)
Function to take into account the effect of the amplification on the simulated SSD waveforms 
It acceps a RDWaveform and a vector contianing the transfer function that can be either simulated or measured 
It returns a new waveform with amplification effects  
"""
function add_amplification(ssd_wf::RDWaveform, channel_transfer_function::AbstractVector)
    #max_pos = argmax(channel_transfer_function)
    new_wf = conv(ustrip.(u"e_au", ssd_wf.signal),channel_transfer_function)[1:length(ssd_wf.signal)]
    return RDWaveform(ssd_wf.time,new_wf.*u"e_au") 
end


"""
    function invert_polarity(ssd_wf::RDWaveform)
Function to invert the polarity of ssd waveforms 
"""
function invert_polarity(ssd_wf::RDWaveform)
    return RDWaveform(ssd_wf.time,-ssd_wf.signal) 
end

"""
    add_electronic_noise(ssd_wf_amp::RDWaveform, e_noise::AbstractVector)
Function to add electronics noise to a waveform  
It acceps a RDWaveform and a vector contianing baseline data from the desired detector
It returns a new waveform containing a new waveform with electronics noise  
"""
function add_electronics_noise(ssd_wf_amp::RDWaveform, e_noise::AbstractVector)
    # Need to normalize the noise taking into account signal to noise ratio
    # To do: need measurements 
    # Need to check if the baseline has at least the same size as the waveforms
    if length(ssd_wf_amp.signal) <= length(e_noise)
        new_wf = ustrip.(u"e_au", ssd_wf_amp.signal) + e_noise[1:length(ssd_wf_amp.signal)]
        return RDWaveform(ssd_wf_amp.time,new_wf.*u"e_au") 
    else
        return "Error: baseline need to have at least the same size as the waveform"
    end
end


"""
    trap_filter(wf::RDWaveform, filter_length::Int=750, window_length::Int=250)
Function to get the reconstructed energy from a waveform   
It acceps a RDWaveform and a filter window that are supposed to have been obtained previously 
It returns ADC value that can be converted into keV
"""
function trap_filter(wf::RDWaveform, filter_length::Int=750, window_length::Int=250, output::Bool = false )
    r1 = 1 : window_length + 1
    r2 = 1 + window_length : 1 + 2*window_length
    r3 = filter_length - window_length : filter_length
    tw = begin
        w = ones(Float64, filter_length)
        w[r2] .= 0
        w[r3] .= -1
        w
    end
    filter = filt(tw, (wf.signal/u"e_au") / window_length);
    if output == false 
        return maximum(filter) - mean(filter[r1]) 
    else 
        return RDWaveform(wf.time, filter.*u"e_au")
    end
end

function trap_filter(wf::Vector, filter_length::Int=750, window_length::Int=250, output::Bool =false )
    r1 = 1 : window_length + 1
    r2 = 1 + window_length : 1 + 2*window_length
    r3 = filter_length - window_length : filter_length
    tw = begin
        w = ones(Float64, filter_length)
        w[r2] .= 0
        w[r3] .= -1
        w
    end
    filter = filt(tw, wf / window_length);
    if output == false 
        return maximum(filter) - mean(filter[r1]) 
    else 
        return filter
    end
end

"""
     pole_zero(wf::RDWaveform, t_tau::T) where {T} 
Function to apply a pole zero correction to a given waveform    
It acceps a RDWaveform andteh decay constant previously measured in samples
It returns a new waveform with flat tail  
"""
function pole_zero(wf::RDWaveform, t_tau::T) where {T} 
    new_signal = zeros(eltype(ustrip(wf.signal)), length(wf.signal))
    sample_to_time = t_tau / ustrip(step(wf.time))
    decay = exp(-1 / sample_to_time)
    new_signal[1] = ustrip(wf.signal[1])
    for i =2:1:length(wf.signal)
        new_signal[i] = new_signal[i-1] + ustrip(wf.signal[i]) -  ustrip(wf.signal[i-1]) * decay
    end
    return RDWaveform(wf.time, new_signal.*u"e_au" )
end


"""
     trap_norm(w_in::RDWaveform, peaking_time::Int, flat_top::Int, output::Bool = false)
Function to apply a trapezoidal filter to a given waveform     
It acceps a RDWaveform and the window for the rising and the flat sections
It returns the difference between the top and bottom part of the trapezoid
"""
function trap_norm(w_in::RDWaveform, peaking_time::Int, flat_top::Int, output::Bool = false) 
    n_samples = length(w_in.signal) 
    if 2 * peaking_time + flat_top > n_samples
	println("FATAL ERROR: 2*peaking_time + flat_top > waveform length")
    end
    window = trunc(Int, n_samples/2)
    min_segment = trunc(Int,window + peaking_time  + flat_top/2 - flat_top/3) 
    max_segment = trunc(Int,window + peaking_time  + flat_top/2 + flat_top/3) 
    r2 = min_segment : max_segment
    w_out = zeros(eltype(ustrip(w_in.signal)), n_samples)
    w_out[1] = (ustrip(w_in.signal[1]))/peaking_time
    for i =2:1:peaking_time
        w_out[i] = w_out[i-1] + (ustrip(w_in.signal[i]))/peaking_time
    end
    for i =peaking_time+1:1:peaking_time + flat_top
        w_out[i] = w_out[i-1] + (ustrip(w_in.signal[i]) - ustrip(w_in.signal[i-peaking_time]))/peaking_time
    end
    for i =peaking_time + flat_top + 1 :1: 2 * peaking_time + flat_top
        w_out[i] = w_out[i-1] + (ustrip(w_in.signal[i]) - ustrip(w_in.signal[i-peaking_time]) - ustrip(w_in.signal[i-peaking_time-flat_top]))/peaking_time
    end
    for i =2 * peaking_time + flat_top + 1:1:n_samples 
        w_out[i] = w_out[i-1] + (ustrip(w_in.signal[i]) - ustrip(w_in.signal[i-peaking_time]) - ustrip(w_in.signal[i-peaking_time-flat_top]) + ustrip(w_in.signal[i-2*peaking_time-flat_top]))/peaking_time
    end
    if output == false 
        return mean(w_out[r2]) 
    else 
    	return w_out
    end
end 

"""
     trap_norm(w_in::Vector, peaking_time::Int, flat_top::Int, output::Bool = false)
Function to apply a trapezoidal filter to a given waveform     
It acceps a RDWaveform and the window for the rising and the flat sections
It returns the difference between the top and bottom part of the trapezoid
"""
function trap_norm(w_in::Vector, peaking_time::Int, flat_top::Int, output::Bool = false) 
    n_samples = length(w_in) 
    if 2 * peaking_time + flat_top > n_samples
	println("FATAL ERROR: 2*peaking_time + flat_top > waveform length. Filter must be shorter than waveform.")
    end
    window = trunc(Int, n_samples/2)
    min_segment = trunc(Int,window + peaking_time  + flat_top/2 - flat_top/3) 
    max_segment = trunc(Int,window + peaking_time  + flat_top/2 + flat_top/3) 
    r2 = min_segment : max_segment
    w_out = zeros(n_samples)
    w_out[1] = w_in[1]/peaking_time
    for i =2:1:peaking_time
        w_out[i] = w_out[i-1] + w_in[i]/peaking_time
    end
    for i =peaking_time+1:1:peaking_time + flat_top
        w_out[i] = w_out[i-1] + (w_in[i] - w_in[i-peaking_time])/peaking_time
    end
    for i =peaking_time + flat_top + 1 :1: 2 * peaking_time + flat_top
        w_out[i] = w_out[i-1] + (w_in[i] - w_in[i-peaking_time] - w_in[i-peaking_time-flat_top])/peaking_time
    end
    for i =2 * peaking_time + flat_top + 1:1:n_samples 
        w_out[i] = w_out[i-1] + (w_in[i] - w_in[i-peaking_time] - w_in[i-peaking_time-flat_top] + w_in[i-2*peaking_time-flat_top])/peaking_time
    end
    if output == false 
	return mean(w_out[r2]) 
    else
    	return w_out
    end
end 


"""
     add_PileUp(wv1::RDWaveform, wv2::RDWaveform, time_shift::Int)
Function to simulate pile up effects      
It acceps two RDWaveforms, the time of the first one is the starting point, t = 0 
while the second event will start few instants later (time_shift) that is given by the user 
The time_shift should be measured from real data and should be randomly generated from a distribution. 
This time distribution is dependent on each setup and experiment
"""
function add_PileUp(wv1::RDWaveform, wv2::RDWaveform, time_shift::Int)
    #the new wf length depends on the time shift and the length of both waveforms
    total_length = length(wv2.signal) + time_shift
    
    new_signal = zeros(eltype(ustrip(wv2.signal[1])),total_length)
    if time_shift < length(wv1.signal)
        for sample = 1:1:total_length
            if sample <= time_shift
                new_signal[sample] = ustrip(wv1.signal[sample])
            elseif time_shift < sample < length(wv1.signal)
                new_signal[sample] = ustrip(wv1.signal[sample]) + ustrip(wv2.signal[sample-time_shift])
            else
                new_signal[sample] = ustrip(wv1.signal[end]) + ustrip(wv2.signal[sample-time_shift])
            end
        end
    else
        for sample = 1:1:total_length
            if sample <= length(wv1.signal)
                new_signal[sample] = ustrip(wv1.signal[sample])
            elseif length(wv1.signal) < sample <= time_shift
                new_signal[sample] = ustrip(wv1.signal[end])
            else
                new_signal[sample] = ustrip(wv1.signal[end]) + ustrip(wv2.signal[sample-time_shift])
            end
        end
    end
    new_times = range( zero(first(wv1.time)), step = step(wv1.time), length = total_length )
    return RDWaveform( new_times, new_signal.*u"e_au" )    
end


"""
     median_filter(data::Vector, filter_size::Int)
Function to perform a median filter, it gets a vector and waveform and returns a new waveform
filtered using the median filter
It accepts a vector and the size of a window
"""
function median_filter(data::Vector{<:Real}, filter_size::Int)
    temp = zeros(filter_size)
    indexer = filter_size ÷ 2
    data_final = zeros(length(data))
    for i =1:1:length(data)
        for z =1:1:filter_size
            if i + z - indexer < 1
                temp[z]=mean(data[i:i+indexer])
            elseif i + z - indexer > length(data)
                temp[z]=data_final[i-1]
            else
                temp[z]=data[i + z - indexer]
            end
        end
        data_final[i] = median(temp)
    end
    return data_final
end

"""
     soleil_filter(wf_ssd::RDWaveform, filter_size::Int, rise_start::Int, rise_ends::Int)
Function to remove bumps in waveform produced by events in beighboring channels and return a waveform with median filter applied
It accepts a wavform, the size of the window to apply the median filter, nd the start and end of the bump
Filter should be applied after identified the main channel and rising time to provide the start and end of event
"""
function soleil_filter(wf_ssd::RDWaveform, filter_size::Int, rise_start::Int, rise_ends::Int)
    temp = zeros(filter_size)
    indexer = filter_size ÷ 2
    data = ustrip.(wf_ssd.signal)
    temp_data = zeros(length(data))
    temp_data[1:rise_start] = data[1:rise_start]
    temp_data[rise_ends:end] = data[rise_ends:end]
    gap_band = (rise_ends - rise_start) ÷ 2
    for gap = rise_start : 1 : rise_ends - gap_band
        if gap - gap_band > 1
            temp_data[gap] = data[gap-gap_band]
        else
            temp_data[gap] = data[1]
        end
    end
    for gap = rise_ends - gap_band : 1 : rise_ends
        if gap + gap_band < length(data)
            temp_data[gap] = data[gap+gap_band]
        else
            temp_data[gap] = data[end]
        end
    end

    data_final = zeros(length(data))
    for i =1:1:length(data)
        for z =1:1:filter_size
            if i + z - indexer < 1
                temp[z]=temp_data[1]
            elseif i + z - indexer > length(data)
                temp[z]=temp_data[end]
            else
                temp[z]=temp_data[i + z - indexer]
            end
        end
        data_final[i] = median(temp)
    end
    return RDWaveform( wf_ssd.time, data_final.*u"e_au" )
end

"""
	moving_window_filter(w_in::Vector, window::Int)
Function to apply a moving average window filter
It accepts a waveform and the size of the window to perform the average
It returns a new waveform
"""
function moving_window_filter(w_in::Vector{<:Real}, window::Int)
    w_out = zeros(length(w_in))
    half_window = window ÷ 2
    for i = 1 : 1 : half_window
        w_out[i] = sum(w_in[i:i+half_window]) / (half_window + 1)
    end
    for i = half_window + 1 : 1 : length(w_in)-half_window
        w_out[i] = sum(w_in[i-half_window:i+half_window]) / (window + 1)
    end
    for i = length(w_in)- half_window + 1 : 1 : length(w_in)
        w_out[i] = sum(w_in[i-half_window:i]) / (half_window + 1)
    end
    return w_out
end

"""
	function find_peaks(x::AbstractVector{T};
    nups::Int=1,
    ndowns::Int=nups,
    zerostr::Char='0',
    peakpat=nothing,
    minpeakheight=typemin(T),
    minpeakdistance::Int=1,
    threshold=zero(T),
    npeaks::Int=0,
    sortstr=false) where T
Function to find peaks in a given array using severla paramters as input as: minimum height of peaks (minpeakheight), threshold, distance bettwen peaks
the minimal working example for a trapezoidal filter is:
find_peaks(x;minpeakheight=350,minpeakdistance=10,threshold=0)	which will return all peaks with a threshold of 350 units (~keV) and separated of at least 10 samples   
"""
function find_peaks(x::AbstractVector{T};
    nups::Int=1,
    ndowns::Int=nups,
    zerostr::Char='0',
    peakpat=nothing,
    minpeakheight=typemin(T),
    minpeakdistance::Int=1,
    threshold=zero(T),
    npeaks::Int=0,
    sortstr=false) where T

    zerostr ∉ ('0', '+', '-') && error("zero must be one of `0`, `-` or `+`")

    # generate the peak pattern with no of ups and downs or use provided one
    peakpat = Regex(peakpat === nothing ? "[+]{$nups,}[-]{$ndowns,}" : peakpat)

    # transform x into a "+-+...-+-" character string
    xs = String(map(diff(x)) do e
        e < 0 && return '-'
        e > 0 && return '+'
        return zerostr
    end)

    # find index positions and maximum values
    peaks = map(findall(peakpat, xs)) do m
        v, i = findmax(@view x[m])
        (;value=v, idx=first(m) + i - 1, start=first(m), stop=last(m) + 1)
    end

    # eliminate peaks that are too low
    filter!(peaks) do p
        p.value >= minpeakheight && p.value - max(x[p.start], x[p.stop]) >= threshold
    end

    # sort according to peak height
    if sortstr || minpeakdistance > 1
        sort!(peaks, by=x -> x.value; rev=true)
    end

    # find peaks sufficiently distant
    if minpeakdistance > 1
        removal = falses(length(peaks))
        for i in 1:length(peaks)
            removal[i] && continue
            for j in 1:length(peaks)
                removal[j] && continue
                dist = abs(peaks[i].idx - peaks[j].idx)
                removal[j] = 0 < dist < minpeakdistance
            end
        end
        deleteat!(peaks, removal)
    end

    # Return only the first 'npeaks' peaks
    npeaks > 0 && resize!(peaks, min(length(peaks), npeaks))

    return peaks
end

"""
        function count_events_above_threshold(A::Vector{<:Real}, threshold::Real, range::Int=4, debug=false)
Function to get the number of events above a threshold in a given range
It takes a waveform and return the number of events above the threshold or a step waveform
"""
function count_events_above_threshold(A::Vector{<:Real}, threshold::Real, range::Int=3, debug=false)
    wf_step = zeros(Int,length(A))
    #first convert to steps
    for i = range+1 : 1 : length(A)-range
        th_break = A[i-range:i+range] .> threshold
        if sum(th_break) == 2*range + 1
            wf_step[i] = 1
        end
    end
    #count the number of steps or events
    counter = 0
    for i = 2 : 1 : length(A)
        if wf_step[i] == 1 && wf_step[i-1] == 0
            counter += 1
        end
    end
    if debug
        return wf_step
    else
        return counter
    end
end
