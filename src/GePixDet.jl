module GePixDet

using UnitfulAtomic
using Unitful
using RadiationDetectorSignals
using DSP
using Statistics
using HDF5
using RadiationSpectra
using DataFrames
using SolidStateDetectors

import Plots
import StatsBase
import LinearAlgebra
import Interpolations
import SpecialFunctions
import Distances
import CSV

export trap_filter
export trap_norm
export median_filter
export pole_zero
export moving_window_filter
export find_peaks
export count_events_above_threshold
export invert_polarity
export get_rising_time
export get_pixel_hitted
export add_baseline_and_extend_decay_tail
export add_amplification
export add_electronics_noise
export add_PileUp
export get_DANTE_data
export get_DANTE_wf_mode
export get_DANTE_decimation
export get_Event_positions 
export get_restart_wf
export get_restart_wf_trap
export correct_leakage_current 
export get_evts_index
export get_wfs_evt
export get_wfs_evt_si
export get_next_dt

include("SoleilFunctions.jl")
include("GePixDSP.jl")
include("DANTE.jl")
include("GePix2G4Connector.jl")


end # module
