"""
	function get_evts_index(df::DataFrame)
Function to get the index of the events, start, end in order to give the event information to SSD
It acepts an output from geant4 with header with the format: evt	x	y	z	E
It returns a matrix with columns: Event_number, Index_evt_start, Index_evt_ends
"""
function get_evts_index(df::DataFrame)
    event_number = df[1,:evt]
    evt_counter = 1
    evts_info = []
    start_evt_index = 1
    for i = 1 : 1 : length(df[!,:evt])
        if  event_number == df[i,:evt]
            continue
        else
            end_evt_index = i - 1
            event_number = df[i,:evt]
            push!(evts_info,[evt_counter start_evt_index end_evt_index])
            evt_counter += 1
            start_evt_index = i
        end
    end
    return vcat(evts_info...)
end


"""
	function get_wfs_evt(ssd_info,df::DataFrame,time_bin=4u"ns",guard_ring::Int=1)
Function to compute the waveforms of an event.
It accepts the SSD output with the info of the fields, a data Frame containing the hits of an event with the format: evt x y z E, as option 1 if there is a guard ring or 0 if there is no guard ring
It returns the waveforms of all readout electrodes except the guard ring if any
"""
function get_wfs_evt(ssd_info,df::DataFrame,time_bin=4u"ns",guard_ring::Int=1)
    wf_evt = Array{ArrayOfRDWaveforms}[]
    time_step = time_bin
    event_coordinates = CartesianPoint{eltype(ssd_info.electric_field[1])}[]
    event_energy = []
    E_evt = 0
    for i = 1 : 1 : length(df[!,:evt])
        energy = df[i,:E]
        hit_coordinates = CartesianPoint{eltype(ssd_info.electric_field[1])}(df[i,:x]/1000,df[i,:y]/1000,(df[i,:z])/1000)
        push!(event_coordinates,hit_coordinates)
        push!(event_energy,energy)
    end
    E_event = event_energy*u"eV"
    evt = Event(event_coordinates, E_event)
    E_evt = sum(event_energy)
    try
        simulate!(evt, ssd_info,Δt = time_step,diffusion = true, self_repulsion = true)
        wf_evt = evt.waveforms[2:end - guard_ring]
    catch
        starting_positions = [ CartesianPoint{eltype(ssd_info.electric_field[1])}( 0.0, 0.0, 0.0 )]
        energy_depos = eltype(ssd_info.electric_field[1])[0.1] * u"keV"
        evt = Event(starting_positions, energy_depos);
        simulate!(evt, ssd_info,Δt = time_step,diffusion = true, self_repulsion = true)
        wf_evt = evt.waveforms[2:end - guard_ring]
        E_evt = 0.1         
    end
    return wf_evt, E_evt
end

"""
	function get_wfs_evt_si(ssd_info,df::DataFrame,time_bin=0.1u"ns")
Function to compute the waveforms of an event.
It accepts the SSD output with the info of the fields, a data Frame containing the hits of an event with the format: evt x y z E.
the time_bin can be adjusted if needed, the recommended value for sensor with thickness of the order of hundreds of microns is 0.1ns.
It returns the waveforms of all readout electrodes 
"""
function get_wfs_evt_si(ssd_info,df::DataFrame,time_bin=0.1u"ns")
    wf_evt = Array{ArrayOfRDWaveforms}[]
    time_step = time_bin
    event_coordinates = CartesianPoint{eltype(ssd_info.electric_field[1])}[]
    event_energy = []
    E_evt = 0
    for i = 1 : 1 : length(df[!,:evt])
        energy = df[i,:E]
        hit_coordinates = CartesianPoint{eltype(ssd_info.electric_field[1])}(df[i,:x]/1000,df[i,:y]/1000,(df[i,:z])/1000)
        push!(event_coordinates,hit_coordinates)
        push!(event_energy,energy)
    end
    E_event = event_energy*u"eV"
    evt = Event(event_coordinates, E_event)
    E_evt = sum(event_energy)
    try
        simulate!(evt, ssd_info,Δt = time_step, self_repulsion = true)
        wf_evt = evt.waveforms[2:end]
    catch
        starting_positions = [ CartesianPoint{eltype(ssd_info.electric_field[1])}( 0.0, 0.0, 0.0 )]
        energy_depos = eltype(ssd_info.electric_field[1])[0.1] * u"keV"
        evt = Event(starting_positions, energy_depos);
        simulate!(evt, ssd_info,Δt = time_step, self_repulsion = true)
        wf_evt = evt.waveforms[2:end]
        E_evt = 0.1         
    end
    return wf_evt, E_evt
end
