# GePixDet

Software to simulate, process and analyze the response of pixelated Germanium detectors under development at synchrotron SOLEIL 

Software designed using the Julia programing language. Updated with julia version 1.9, should run in SUMO cluster of SOLEIL without problems. 

First you will need to charge the following modules 

```
module load tools/python/anaconda3/5.3
#julia staff, first define the number of threads you want to use. 
#It might be more convenient to define a depoth directory different than the default home
export JULIA_NUM_THREADS=24
export JULIA_DEPOT_PATH=/lustre/work/experiences/detecteurs/your_user_name/.julia
#Uncomment next line if you want to use the installed julia module 
#module load tools/julia/1.9.0
#To use your own julia versions just indicate the path 
export PATH=$PATH:/home/experiences/detecteurs/your_user_name/julia-1.9.1/bin
```




You can put these instruction in a setup_gepixdet.sh file and then just load it when connected to sumo by doing: 

`source setup_gepixdet.sh`


To open and run the example notebook in SUMO cluster, follow these instructions: 
1. Connect with ssh and forward port:
`ssh -L 1234:localhost:1234 sumo`
2. Charge anaconda module: 
`source setup_gepixdet.sh`
If it's the firt time you are using julia kernel in the notebook, the first step is to install the IJulia module, which is needed to use jupyter notebooks with julia. Just do this in a terminal 
```
julia 
]add IJulia
```
You have sime intructions to install julia and julia packages in sumo below. 

3. You can start now in the terminal jupyter notebook

`jupyter notebook --no-browser --port 1234`

4. Copy link in your browser and run notebook 

# Simulation tutorials 

Two notebooks containing step by step instructions have been added to the `notebook_tutorials/` of this repository 

1) `LEAPS_fields_SSD_step_by_step_example.ipynb` notebook contains all the instructions that you need to calculate the fields with SSD 

2) `LEAPS_step_by_step_with_pile_up_chain.ipynb` notebook contains all the instructions that you need to use the field containing the information about the fields with SSD, read a Geant4 file, extract the events informnation, generate waveforms, include amplification, add electronic noise, pile-up, etc 

## Si pixelated detector 

Simulations with pixelated detector can also be simulated with this package. Two tutorials have been included in the  `notebook_tutorials/` of this repository  

1) `UFXC_regular_pixel_config_file.ipynb` can be used to simulate the fields. You can llok at the notebook and adapt it to your needs 

2) `UFXC_Si_mobility.ipynb` use the previously calculated fields and show how to define a charge carrier model for silicon. you can use it as exaple to define any charge carrier mobility model. 

# Instructions to install and use julia in SUMO cluster 

In SUMO you have already installed julia. However you can easily install the latest version of julia. You just need to connect to sumo and run in the terminal 
```
wget https://julialang-s3.julialang.org/bin/linux/x64/1.9/julia-1.9.3-linux-x86_64.tar.gz 
tar zxvf julia-1.9.3-linux-x86_64.tar.gz
```
You can update the instructions in function of the latest julia version. The instructions with the latest julia version are available here: https://julialang.org/downloads/platform/

Then just update the instructions described at the beginning of this readme

# Instructions to install and use julia in TOPAZE computing center 

If you want to run the simulation chain in topaze computing center you have to do the following:

1) Copy the `julia-1.9.3-linux-x86_64.tar.gz`  compressed file that you have download for SUMO to your home directory in topaze. Then just do `tar zxvf julia-1.9.3-linux-x86_64.tar.gz` 

2)Once you have installed all the packages that are required to run the full simulation chain in sumo you have to copy the `.julia` file to topaze. You can do: 
```
zip -r julia_pkg.zip .julia
scp julia_pkg.zip your_user_name@topaze.ccc.cea.fr:/ccc/work/cont003/soleil/your_user_name/
``` 
Then connect to topaze and do: 
```
ssh -XY your_user_name@topaze.ccc.cea.fr
cd /ccc/work/cont003/soleil/your_user_name/
unzip julia_pkg.zip
```


After you have done this, you can define a setup_ssd.sh file containing the following:
```
module load gnu/11.1.0
export JULIA_NUM_THREADS=24
export JULIA_DEPOT_PATH=/ccc/work/cont003/soleil/your_user_name/.julia
export PATH=$PATH:/ccc/cont003/home/soleil/your_user_name/julia/julia-1.9.0/bin/
```

and then just run `source setup_ssd.sh` when you connect to topaze

## Installing a package in julia   

If it is the first time running the code you will need to install some required packages. To install a package just open julia, in a terminal:

```
julia
]add MyPackageName
```


For example if you want to install the package Plots, or this packages from the gitlab page: 

```
]add Plots
]add https://gitlab.synchrotron-soleil.fr/detecteurs/leaps-innov 
```

Another option to install dedicated software is directly in the ntoebook or in julia by doing:
```
julia
using Pkg
Pkg.add(url="https://gitlab.synchrotron-soleil.fr/detecteurs/leaps-innov")
```


You can also clone this or another package and then add it directly from a local repository
